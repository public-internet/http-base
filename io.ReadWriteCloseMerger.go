// This is suprisingly not in the standard library
package httpbase
import "io"

type readWriteCloseMerger struct{
	reader io.Reader
	writer io.Writer
	closer io.Closer
}

func ( self readWriteCloseMerger )Read( buffer [ ]byte )( int , error ) {
	return self.reader.Read( buffer )
}

func ( self readWriteCloseMerger )Write( buffer [ ]byte )( int , error ) {
	return self.writer.Write( buffer )
}

func ( self readWriteCloseMerger )Close( )error {
	return self.closer.Close( )
}

