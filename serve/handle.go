package httpbase
import "io"
import "net"
import "net/http"

// Handle
type Handle struct{
//	egress paserver.Egress
	Egress func( io.ReadWriteCloser )error
}
func ( self Handle )ServeHTTP( response http.ResponseWriter , request * http.Request ) {
	var err error
	var ok bool
	var hijack http.Hijacker
	var socket net.Conn

	// Check request
	for _ , headerConnect := range request.Header[ "Connection" ] {
		for _ , headerUpgrade := range request.Header[ "Upgrade" ] {
			if	( headerConnect == "Upgrade" ||
				  headerConnect == "upgrade" ) &&
				( headerUpgrade == "TCPOMUX-START" ||
				  headerUpgrade == "tcpomux-start" ) {
				ok = true }
		}
	}
	if ! ok {
		http.FileServer( http.Dir( "." ) ).ServeHTTP( response , request )
		return
	}

	// Hijack
	hijack , ok = response.( http.Hijacker )
	if ! ok {
		response.WriteHeader( http.StatusInternalServerError )
		response.Write( [ ]byte( "hijack not supported" ) )
		return
	}
	socket , _ , err = hijack.Hijack( )
	if err != nil {
		response.WriteHeader( http.StatusInternalServerError )
		response.Write( [ ]byte( err.Error( ) ) )
		return
	}

	// Pass to egress
	if self.Egress == nil {
		socket.Write( [ ]byte( "HTTP/1.1 500 Internal Server Error\r\n\r\n" ) )
		return
	}
	_ , err = socket.Write( [ ]byte( "HTTP/1.1 101 Switching Protocols\r\nConnection: upgrade\r\nUpgrade: tcpomux-start\r\n\r\n" ) )
	if err != nil {
		return }
	_ = self.Egress( socket )

	return
}

