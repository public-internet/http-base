package httpbase
import "io"
import "net"
import "net/http"

func Serve( ipaddr net.IP , egress func( io.ReadWriteCloser )error )error {
	return ( & http.Server{
		Handler : Handle{
			Egress : egress ,
		} ,
		Addr : "[" + ipaddr.String( ) + "]:80" ,
	} ).ListenAndServe( )
}


