// Public Access. Copyright (c) 2021 Kona Arctic. All rights reserved. ABSOLUTELY NO WARRANTY! https://akona.me mailto:arcticjieer@gmail.com
// Generic client library for HTTP based methods
package httpbase
import "bufio"
import "crypto/tls"
import "errors"
import utls "github.com/refraction-networking/utls"
import "io"
import "net"
import "net/http"
import "net/url"

type Base struct{
	url.URL
	Query string
	Host string
	Utls bool
	Outer string
	Inner string
	Upgrade string
}

func ( self Base )Connect( )( io.ReadWriteCloser , error ) {
	var err error
	var socket net.Conn
	var stream readWriteCloseMerger

	// Make connection
	if self.Utls {
		socket , err = net.Dial( "tcp" , ternary( self.Host != "" , self.Host , self.Outer ).( string ) + ":443" )
		if err != nil {
			return socket , err }
		socket = utls.UClient( socket , & utls.Config{
			ServerName: ternary( self.Outer != "" , self.Outer , self.Host ).( string ),
		} , utls.HelloRandomizedNoALPN )
	} else {
		socket , err = tls.Dial( "tcp" , ternary( self.Host != "" , self.Host , self.Outer ).( string ) + ":443" , & tls.Config{ 
			ServerName: ternary( self.Outer != "" , self.Outer , self.Host ).( string ),
			MinVersion: 3,
		} )
		if err != nil {
			return socket , err }
	}
	stream.reader = bufio.NewReader( socket )
	stream.writer = socket
	stream.closer = socket

	// HTTP
	request := http.Request{
		Method: "GET",
		URL: & url.URL{
			Scheme: "http",
			Host: ternary( self.Inner != "" , self.Inner , self.Host ).( string ),
			Path: "/" + self.Path,
			RawQuery: self.Query,
		},
		ProtoMajor: 1,
		ProtoMinor: 1,
		Header: map[ string ][ ]string{
			"Connection": [ ]string{ "upgrade" },
			"Upgrade": [ ]string{ ternary( self.Upgrade != "" , self.Upgrade , "tcpomux-start" ).( string ) },
			"User-Agent": [ ]string{ "PublicInternet/0.3" },
		},
		Close: false,
	}
	err = request.Write( stream )
	if err != nil {
		return socket , err }
	response , err := http.ReadResponse( stream.reader.( * bufio.Reader ) , & request )
	if err != nil {
		return socket , err }
	if response.StatusCode != 101 {
		return socket , errors.New( "server returned " + response.Status ) }

	return stream , nil
}


