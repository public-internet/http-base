module gitlab.com/public-internet/http-base

go 1.15

require (
	github.com/refraction-networking/utls v1.0.0
	golang.org/x/crypto v0.0.0-20210711020723-a769d52b0f97 // indirect
	golang.org/x/sys v0.0.0-20210630005230-0f9fa26af87c // indirect
)
